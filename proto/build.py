import subprocess
import os

class Builder:
    """
    The Base Builder Class.
    """
    def __init__(self, dir:str) -> None:
        """
        Constructs the `Builder` class.
        
        @params

        dir (str) --> The directory where the build is to take place.
        """
        if not os.path.exists(dir):
            print("Invalid path: {}".format(dir))
            quit()

        os.chdir(dir)

        if "go.mod" not in os.listdir():
            print("!!!!!!! BUILD FAILED !!!!!!!\n`go.mod` file not found in current working directory.")
            quit()

        self.project_name = dir.split("\\")[-1]
        self.platforms = ["windows", "linux", "darwin"]

    def build(self):
        """
        Builds the executable for different OS.
        """
        print("Starting cross-builds...")

        try:
            for i in self.platforms:
                print(f"Building {i} executable...")
                bash_build = subprocess.Popen(["bash"], stdin=subprocess.PIPE, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
                command = f"env GOOS={i} GOARCH=amd64 go build -o bin/{self.project_name}-{i}-amd64"
                if i == "windows":
                    command += ".exe"
                _ = bash_build.communicate(command.encode("utf-8"))
                bash_build.terminate()

            self.check_build_success()

        except Exception as e:
            print(f"!!!!!!! BUILD FAILED !!!!!!")
            print(e)

        else:
            print("!!!!!!!!! ALL CROSS-BUILDS SUCCESSFUL !!!!!!!!!")


    def check_build_success(self):
        for i in self.platforms:
            executable = f"{self.project_name}-{i}-amd64"
            if i == "windows":
                executable += ".exe"
            assert (executable in os.listdir("./bin"))

if __name__ == "__main__":
    builder = Builder(r"C:\Users\Admin\Documents\omega")
    builder.build()